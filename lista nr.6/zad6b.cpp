#include <iostream>
#include <cstdlib>
#define dlNazwa 100

using namespace std;

int id = 0;

struct Pojazd
{   
    int id;
    int w;
    int k;
    char nazwa[dlNazwa];
    int ** miejsce;
    Pojazd * nastepny;
};

Pojazd * dodajNowyPojazd(Pojazd * lista)
{  
    Pojazd* bus = new Pojazd;
    int wiersze, kolumny;
    
    cin.ignore(cin.rdbuf()->in_avail()); cin.get();
    cout << "Podaj nazwe pojazdu: "; cin.getline(bus->nazwa, dlNazwa);
    cout << "Ile jest rzedow: "; cin >> wiersze;
    cout << "Ile miejsc w rzedzie: "; cin >> kolumny;
    system("clear");

    if(wiersze < 0 || kolumny < 0)
    {
        cout << "Wprowadzono bledne dane\n\n";
        return lista;
    }
        

    bus->miejsce = new int*[wiersze];
    for (int i = 0; i < wiersze; i++)
        bus->miejsce[i] = new int[kolumny];

    for (int i = 0; i < wiersze; i++)
    {
        for (int j = 0; j < kolumny; j++)
        {
            bus->miejsce[i][j] = 0;
        }
    }

    bus->id = id; id++;
    bus->w = wiersze;
    bus->k = kolumny;
    bus->nastepny = lista;
    return bus;
}

void wyswietlPodglad(Pojazd * biezacy)
{  
    while(biezacy != NULL)
    {
        cout << biezacy->nazwa << " (id: " << biezacy->id << ")\n";
            
        cout << " | ";
        for (int j = 0; j < biezacy->k; j++)
            cout << j << " ";

        cout << endl << "-|-";
        for (int j = 0; j < biezacy->k; j++)
            cout << "--";
        cout << endl;

        for (int i = 0; i < biezacy->w; i++)
        {
            cout << i << "| ";
            for (int j = 0; j < biezacy->k; j++)
            {
                cout << biezacy->miejsce[i][j] << " ";
            }
            cout << endl;
        }
        cout << endl;
        biezacy = biezacy->nastepny;
    }
}

int wyswietlPojazd(Pojazd * biezacy, int idWybranegoPojazdu)
{
    cout << endl;
    while(biezacy != NULL)
    {
        if(biezacy->id == idWybranegoPojazdu)
        {
            cout << biezacy->nazwa << " (id: " << biezacy->id << ")\n";

            cout << " | ";
            for (int j = 0; j < biezacy->k; j++)
                cout << j << " ";

            cout << endl << "-|-";
            for (int j = 0; j < biezacy->k; j++)
                cout << "--";
            cout << endl;

            for (int i = 0; i < biezacy->w; i++)
            {
                cout << i << "| ";
                for (int j = 0; j < biezacy->k; j++)
                {
                    cout << biezacy->miejsce[i][j] << " ";
                }
                cout << endl;
            }
            cout << endl;
            return 0;
        }
        biezacy = biezacy->nastepny;
    }
    return 1;
}

int dodajRezerwacje(Pojazd * biezacy, int idWybranegoPojazdu, int w, int k)
{
    while(biezacy != NULL)
    {
        if(biezacy->id == idWybranegoPojazdu)
        {
            if(biezacy->w > w && biezacy->k > k && w > -1 && k > -1)
            {
                if(biezacy->miejsce[w][k] == 0)
                    biezacy->miejsce[w][k] = 1;
                else 
                    biezacy->miejsce[w][k] = 0;

                return 0;
            }
            else
                return 1;
        }
        biezacy = biezacy->nastepny;
    }
    return 1;
}

int zarezerwujMiejsce(Pojazd * lista)
{  
    int ilePojazdow = 0, idWybranegoPojazdu, w, k;
    cout << "Twoje pojazdy: ";

    Pojazd * biezacy = lista;
    while(biezacy != NULL)
    {
        cout << biezacy->nazwa << " (id: " << biezacy->id << "), ";
        biezacy = biezacy->nastepny;
        ilePojazdow++;
    }

    if(!ilePojazdow)
    {
        cout << "Nie posiadasz zadnych pojazdow!\n\n";
        return 1;
    }
    else
    {
        cout << endl;
        cin.ignore(cin.rdbuf()->in_avail()); cin.get();
        cout << "Podaj id pojazdu: "; cin >> idWybranegoPojazdu;

        biezacy = lista;
        if(wyswietlPojazd(biezacy, idWybranegoPojazdu))
        {
            cout << "Nie istnieje pojazd o id: " << idWybranegoPojazdu << "\n\n";
            return 1;
        }

        cout << "Podaj wiersz i kolumnę (oddzielone spacja): "; cin >> w >> k;
        if(dodajRezerwacje(biezacy, idWybranegoPojazdu, w, k))
        {
            cout << "Wprowadzono bledne dane!\n\n";
            return 1;
        }
            
    }
    return 0;
}

void zwolnijPamiec(Pojazd * biezacy)
{
    while(biezacy != NULL)
    {
        Pojazd * usun = biezacy;
        for(int i = 0; i < biezacy->w; i++)
        {
            delete [] biezacy->miejsce[i];
        }
        delete [] biezacy->miejsce;
        biezacy = biezacy->nastepny;
        delete usun;
    }
}


int main()
{
    Pojazd * lista = NULL;

    while(true)
    {
        int wybor;
        cout << "1. Dodaj pojazd\n";
        cout << "2. Podglad miejsc w pojazdach\n";
        cout << "3. Dodaj/usun rezerwacje\n";
        cout << "0. Zamknij\n";
        cout << "Wybierz: "; cin >> wybor;
        
        system("clear");

        switch(wybor)
        {
            case 0:
                zwolnijPamiec(lista);
                lista = NULL;
                return 0;
                break;

            case 1:
                lista = dodajNowyPojazd(lista);
                break;

            case 2:
                wyswietlPodglad(lista);
                break;
            case 3:
                zarezerwujMiejsce(lista);
                break;

            default:
                cout << "nie ma takiej opcji" << endl;
                break;
        }
    }
}