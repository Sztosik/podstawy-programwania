// dodać numeracje wierszy i kolumn

#include <iostream>

using namespace std;

void zwolnijPamiec(int *** pojazdy, int ilePojazdow, int ileWierszy)
{
    for(int i = 0; i < ilePojazdow; i++)
    {
        for(int j = 0; j < ileWierszy; j++)
        {
            free(pojazdy[i][j]);
        }
    }
    for(int i = 0; i < ilePojazdow; i++)
    {
        free(pojazdy[i]);
    }
    free(pojazdy);
    pojazdy = NULL;
}

void podgladMiejsc(int *** pojazdy, int ilePojazdow, int ileWierszy, int ileKolumn)
{
    for(int i = 0; i < ilePojazdow; i++)
    {
        cout << "Pojazd " << i+1 << endl;

        cout << " | ";
        for (int j = 0; j < ileKolumn; j++)
            cout << j+1 << " ";

        cout << endl << "-|-";
        for (int j = 0; j < ileKolumn; j++)
            cout << "--";
        cout << endl;

        for(int j = 0; j < ileWierszy; j++)
        {
            cout << j+1 << "| ";
            for(int k = 0; k < ileKolumn; k++)
            {
                cout << pojazdy[i][j][k] << " ";
            }
            cout << endl;
        }
        cout << endl;
    }
}

void podgladRezerwacja(int *** pojazdy, int ilePojazdow, int ileWierszy, int ileKolumn)
{
    for(int i = 0; i < ileWierszy; i++)
    {
        for(int j = 0; j < ilePojazdow; j++)
        {  
            for(int k = 0; k < ileKolumn; k++)
            {
                cout << pojazdy[j][i][k] << " ";
            }
            cout << " ";
        }
        cout << endl;
    }
}

int zarezerwujMiejsce(int *** pojazdy, int ilePojazdow, int ileWierszy, int ileKolumn)
{
    int idPojazdu, w, k;
    podgladMiejsc(pojazdy, ilePojazdow, ileWierszy, ileKolumn);
    cout << "Podaj id pojazdu: "; cin >> idPojazdu;
    idPojazdu--;
    if(idPojazdu < 0 || idPojazdu >= ilePojazdow)
    {
        cout << "Nie istnieje pojazd o id: " << idPojazdu+1 << "\n\n";
        return 1;
    }

    cout << "Podaj wiersz i kolumnę (oddzielone spacja): "; cin >> w >> k;
    w--; k--;
    if(ileWierszy > w && ileKolumn > k && w > -1 && k > -1)
    {
        if(pojazdy[idPojazdu][w][k])
            pojazdy[idPojazdu][w][k] = 0;
        else
            pojazdy[idPojazdu][w][k] = 1;
    }
    else
    {
        cout << "Wprowadzono bledne dane!\n\n";
        return 1;
    }

    return 0;
}

int main()
{   
    int ilePojazdow, ileWierszy, ileKolumn;
    int ***pojazdy = NULL;

    cout << "ile posiadasz pojazdow: "; cin >> ilePojazdow;
    cout << "Ile jest rzedow: "; cin >> ileWierszy;
    cout << "Ile jest miejsc w rzedzie: "; cin >> ileKolumn;

    system("clear");

    pojazdy = (int***) malloc(ilePojazdow * sizeof(int*));

    for(int i = 0; i < ilePojazdow; i++)
    {
        pojazdy[i] = (int**) malloc(ileWierszy * sizeof(int*));
        for(int j = 0; j < ileWierszy; j++)
        {
            pojazdy[i][j] = (int*) calloc(ileKolumn, sizeof(int));
        }
    }

    while(true)
    {
        int wybor;
        cout << "1. Podglad miejsc w pojazdach\n";
        cout << "2. Dodaj/usun rezerwacje\n";
        cout << "0. Zamknij\n";
        cout << "Wybierz: "; cin >> wybor;
        
        system("clear");

        switch(wybor)
        {
            case 0:
                zwolnijPamiec(pojazdy, ilePojazdow, ileWierszy);
                return 0;
                break;

            case 1:
                podgladRezerwacja(pojazdy, ilePojazdow, ileWierszy, ileKolumn);
                break;

            case 2:
                zarezerwujMiejsce(pojazdy, ilePojazdow, ileWierszy, ileKolumn);
                break;

            default:
                cout << "nie ma takiej opcji" << endl;
                break;
        }
    }
}