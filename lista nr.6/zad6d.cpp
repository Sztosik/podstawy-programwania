#include <stdlib.h>
#include <iostream>

using namespace std;

void zwolnij_tab(void **A) 
{ 
    if (!A) return;
    for (int i=0;A[i];i++) free(A[i]); free(A);
}

void ** przydziel_tab(int M, int N, int element_size) 
{ 
    void **A;
    if ((A=(void**)calloc(M+1,sizeof(void *)))==NULL) 
        return NULL;

    for (int i=0;i<M;i++)
        if(!(A[i]=calloc(N,element_size))) goto BLAD;

    A[M]=NULL;
    return A;

    BLAD: zwolnij_tab(A);
    return NULL;
} 

int main() 
{ 
    int **pojazdy;

    if ((pojazdy=(int**)przydziel_tab(100000000, 5, sizeof(int)))==NULL) exit(1);
    cout << "Udalo sie zaalokowac pamiec\n";
    pojazdy[4][3]=3;

    cout << "Element1=" << pojazdy[3][3] << "\n";
    cout << "Element2=" << pojazdy[4][3] << "\n";

    zwolnij_tab((void **)pojazdy);

    return 0;
}