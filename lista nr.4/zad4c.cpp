#include "iostream"
#include "cstdlib"
#define rozmiar 50

using namespace std;
 
void sortowanie(double N[], int rozm);

int main()
{   
    system("clear");
    double N[rozmiar];

    //sprawdza rozmiar tablicy
    int rozm = sizeof(N) / sizeof(N[0]);

    //wypełnia teblicę losowymi liczbami z przedziału 0-99
    for(int i = 0; i < rozm; i++)
    {
        N[i] = rand()%100;
    }
   
    //wyświetla aktualny stan
    cout << "stan tablicy przed sortowaniem" << endl;
    for (int i = 0; i < rozm; i++) {
        cout << N[i];
        cout << " ";
    }
    cout << endl << endl;
 
    //wywołanie funkcji sortowanie
    sortowanie(N, rozm);
 
    //stan tblicy po sortowaniu
    cout << "stan tablicy po sortowaniu" << endl;
    for (int i = 0; i < rozm; i++) {
        cout << N[i];
        cout << " ";
    }
    cout << endl;
    return 0;
}

void sortowanie(double N[], int rozm) {
 
    bool zm = true;
    while(zm) 
    {
        zm = false;
        for (int i = 0; i < rozm-1; i++) {
            //warunek zamieniający liczby, jeślii nie ma liczb do zamienienia to tablica jest posortowana
            if (N[i] > N[i + 1]) {
                int temp = N[i];
                N[i] = N[i + 1];
                N[i + 1] = temp;
 
                zm = true;
            }
        }
    }
}