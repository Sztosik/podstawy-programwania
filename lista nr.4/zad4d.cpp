
 
#include <iostream>
#include <cstdlib>

using namespace std;
 
int partycjonowanie(int N[], int index_poczatkowy, int index_koncowy);
void quicksort(int N[], int index_poczatkowy, int index_koncowy);
 
int main()
{
    system("clear");

    int rozmiar;
    cout << "Podaj rozmiar tablicy N: "; cin >> rozmiar;
    int *N = new int [rozmiar];

    //wypełnia tablice losowymi liczbami 0-99
    for(int i = 0; i < rozmiar; i++)
        N[i] = rand()%100;

    //wyświetlenie tablicy przed sortowaniem
    for (int i = 0; i < rozmiar; i++)
        cout << " " << N[i];

    cout << endl;
    
    quicksort(N, 0, rozmiar-1);
    
    //wyświetlenie tablicy po sortowaniu
    for (int i = 0; i < rozmiar; i++)
        cout << " " << N[i];
    
    delete [] N;
    
    return 0;
}

int partycjonowanie(int N[], int index_poczatkowy, int index_koncowy)
{
    int pivot = N[index_poczatkowy];
    int i = index_poczatkowy;
    int j = index_koncowy;
    int temp;

    while (true)
    {
        while (N[j] > pivot)
            j--;
        while (N[i] < pivot)
            i++;
        if (i < j)
        {
            temp = N[i];
            N[i] = N[j];
            N[j] = temp;
            i++;
            j--;
        }
        else
            return j;
    }
}
 
void quicksort(int N[], int index_poczatkowy, int index_koncowy)
{
    int q;
    if (index_poczatkowy < index_koncowy)
    {  
        q = partycjonowanie(N, index_poczatkowy, index_koncowy);
        quicksort(N, index_poczatkowy, q);
        quicksort(N, q+1, index_koncowy);
    }
}