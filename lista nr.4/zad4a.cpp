#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{   
    int tab[95], rozmiar;
    char napis[1000];

    system("clear");
    cout << "napisz cos: "; 
    cin.getline(napis, 1000);
    cout << napis << endl;

    //zerowanie tablicy
    for(int i = 0; i < 95; i++){
        tab[i] = 0;
    }

    //sprawdzanie dlugości napisu
    for(rozmiar = 0; napis[rozmiar] != '\0'; rozmiar++);

    //zliczanie znaków
    for(int i = 0; i < rozmiar; i++){
        int znak = (int) napis[i];
        tab[znak-32]+=1;
    }

    //wyświetlanie wyniku
    for(int i = 0; i < 95; i++){
        if(tab[i] == 0) continue; 
        char znak = (char) i+32;
        cout << znak << ": " << tab[i] << endl;
    }

    return 0;
}

