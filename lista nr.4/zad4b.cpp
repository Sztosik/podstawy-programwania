#include <iostream>
#include <cstdlib>
#include <ctime>

//wybrałem taki rozmiar żeby móc wygodnie przedlądać tabele i sprawdzać działanie programu
#define rozmiar 50

using namespace std;

int szukaj(int szukana, int tab[], int i, int rozm);

int main()
{   
    int tab[rozmiar], szukana, k=0;
    system("clear");
    srand(time(NULL));

    //sprawdza rozmiar tablicy
    int rozm = sizeof(tab) / sizeof(tab[0]);

    //wypełnia teblicę losowymi liczbami z przedziału 0-99
    for(int i = 0; i < rozm; i++)
    {
        tab[i] = rand()%100;
    }

    //wyświetla tablice
    for(int i = 0; i < rozm; i++)
    {
        cout << tab[i] << " ";
    }
    cout << endl;

    //szuka elementu podanego przez użytkownika i wyświetla wynik
    cout << "czego szukasz: "; cin >> szukana;
    int pozycja = szukaj(szukana, tab, k, rozm);
    if(pozycja != -1)
        cout << "liczba " << szukana << " znajduje sie na pozycji: " << pozycja+1 << endl;
    else
        cout << "taka liczba nie wystepuje w tablicy :(" << endl;

    return 0;
}
int szukaj(int szukana, int tab[], int i, int rozm)
{
    if (i > rozm) {
        return -1; //zwraca liczbę która nie jest indeksem tablicy
    }
    if (szukana == tab[i]) {
        return i; //zwraza indeks szukanego elementu
    }
    else
    {
        return szukaj(szukana, tab, i + 1, rozm);
    }
}