#include <iostream>
#include <ctime>
#include <cstdlib>
#include "math.h"

using namespace std;

struct pizza 
{
    double liczba;
    int wystapienia;
};

void uzupelnij_tab(double tab[], int rozmiar);
void wyswietl_tab(double tab[], int rozmiar);
int partycjonowanie(double N[], int index_poczatkowy, int index_koncowy);
void quicksort(double N[], int index_poczatkowy, int index_koncowy);
double srednia_arytmetyczna(double tab[], int rozmiar);
double srednia_geometryczna(double tab[], int rozmiar);
double srednia_harmoniczna(double tab[], int rozmiar);
double srednia_Kwadratowa(double tab[], int rozmiar);
double mediana(double tab[], int rozmiar);
int tab_bez_powtorzen(double tab[],  int rozmiar, pizza dominanta[]);
int max(pizza dominanta[], int rozmiar);

int main()
{
    srand(time(NULL));
    system("clear");

    bool zm = true, czy_rowne_kawalki = true;
    double* tab = NULL;
    double wynik[5][2];
    int rozm, rozm_pizzy, max_wystapienia;

    cout << "Podaj rozmiar tablicy: "; cin >> rozm;
    if(rozm <= 0)
    {
        cout << "rozmiar musi być liczba dodatnia!" << endl;
        return 0;
    }
    tab = new double[rozm];
    pizza* dominanta = new pizza[rozm];

    uzupelnij_tab(tab, rozm);
    quicksort(tab, 0, rozm-1);
    system("clear");
    wyswietl_tab(tab, rozm);

    wynik[0][0] = srednia_arytmetyczna(tab, rozm);
    wynik[1][0] = srednia_geometryczna(tab, rozm);
    wynik[2][0] = srednia_harmoniczna(tab, rozm);
    wynik[3][0] = srednia_Kwadratowa(tab, rozm);
    wynik[4][0] = mediana(tab, rozm);

    for(int i = 0; i < 5; i++)
        wynik[i][1] = i+1;

    // sortuje bąbelkowo po zerowej kolumnie
    while(zm) 
    {
        zm = false;
        for (int i = 0; i < 5-1; i++) {
            if (wynik[i][0] > wynik[i + 1][0]) {
                swap(wynik[i][0],wynik[i+1][0]);
                swap(wynik[i][1],wynik[i+1][1]);
                zm = true;
            }
        }
    }

    // jako, że dominantą może być naraz więcej niż jedna liczba to nie sortuje dominanty z resztą wyników
    cout << "\n\n";
    for(int i = 0; i < 5; i++)
    {
        if(wynik[i][1] == 1)
            cout << "srednia arytmetyczna: " << wynik[i][0] << endl;
        if(wynik[i][1] == 2)
            cout << "srednia geometryczna: " << wynik[i][0] << endl;
        if(wynik[i][1] == 3)
            cout << "srednia harmoniczna:  " << wynik[i][0] << endl;
        if(wynik[i][1] == 4)
            cout << "srednia Kwadratowa:   " << wynik[i][0] << endl;
        if(wynik[i][1] == 5)
            cout << "mediana:              " << wynik[i][0] << endl;
    }

    // wyświetlenie dominanty
    rozm_pizzy = tab_bez_powtorzen(tab, rozm, dominanta);
    max_wystapienia = max(dominanta, rozm_pizzy);
    //sprawdza czy istnieje dominanta
    for(int i = 0; i < rozm_pizzy; i++)
    {
        if(max_wystapienia != dominanta[i].wystapienia)
        {
            czy_rowne_kawalki = false;
            break;
        }
            
    }
    cout << "\nDominanta: ";
    if(czy_rowne_kawalki)
            cout << "brak";
    else
    {
        for (int i = 0; i < rozm_pizzy; i++)
        {
            if(max_wystapienia == dominanta[i].wystapienia)
                cout << dominanta[i].liczba << ", ";
        }
    }

    cout << "\n\n";
    //Pokazuje ilość wystąpień liczb
    for (int i = 0; i < rozm_pizzy; i++)
    {
        cout << "liczba " << dominanta[i].liczba << " wystapienia: " << dominanta[i].wystapienia << endl;
    }

    delete [] tab;
    delete [] dominanta;
    return 0;
}

void uzupelnij_tab(double tab[], int rozmiar)
{
    for(int i = 0; i < rozmiar; i++)
    {
        tab[i] = rand()%10+1;
    }
}

void wyswietl_tab(double tab[], int rozmiar)
{
    for(int i = 0; i < rozmiar; i++)
    {
        cout << tab[i] << " ";
    }
}

int partycjonowanie(double N[], int index_poczatkowy, int index_koncowy)
{
    int pivot = N[index_poczatkowy];
    int i = index_poczatkowy;
    int j = index_koncowy;
    int temp;

    while (true)
    {
        while (N[j] > pivot)
            j--;
        while (N[i] < pivot)
            i++;
        if (i < j)
        {
            temp = N[i];
            N[i] = N[j];
            N[j] = temp;
            i++;
            j--;
        }
        else
            return j;
    }
}
 
void quicksort(double N[], int index_poczatkowy, int index_koncowy)
{
    int q;
    if (index_poczatkowy < index_koncowy)
    {  
        q = partycjonowanie(N, index_poczatkowy, index_koncowy);
        quicksort(N, index_poczatkowy, q);
        quicksort(N, q+1, index_koncowy);
    }
}

double srednia_arytmetyczna(double tab[], int rozmiar)
{
    double suma = 0;
    for(int i = 0; i < rozmiar; i++)
    {
        suma += tab[i];
    }
    return suma/rozmiar;
}

double srednia_geometryczna(double tab[], int rozmiar)
{
    double srednia = 1;
    for(int i = 0; i < rozmiar; i++)
    {
        srednia *= pow(tab[i], (1.0/rozmiar));
    }
    return srednia;
}

double srednia_harmoniczna(double tab[], int rozmiar)
{
    long double suma = 0;
    for(int i = 0; i < rozmiar; i++)
    {
        suma += 1.0/tab[i];
    }
    return rozmiar/suma;
}

double srednia_Kwadratowa(double tab[], int rozmiar)
{
    long double suma = 0;
    for(int i = 0; i < rozmiar; i++)
    {
        suma += tab[i]*tab[i];
    }
    return sqrt(suma/rozmiar);
}

double mediana(double tab[], int rozmiar)
{
    quicksort(tab, 0, rozmiar-1);

    if(rozmiar % 2 == 0)
        return (tab[rozmiar/2]+tab[(rozmiar/2)-1])/2.0;
    else
        return tab[((rozmiar+1)/2)-1];
}

int tab_bez_powtorzen(double tab[],  int rozmiar, pizza dominanta[])
{
    int wystapienia = 0, licznik=0;
    bool wstawic = true;

    quicksort(tab, 0, rozmiar-1);
    

    for (int i = 0; i < rozmiar; i++)
    {
        dominanta[i].liczba = 0;
    }  
    for(int i = 0; i < rozmiar; i++)
    {
        wstawic = true;
        for(int j = i; j < rozmiar; j++)
        {
            if(tab[j]==tab[i]) wystapienia++;
        }
        for(int j = 0; j < i && dominanta[j].liczba != 0; j++)
        {
            if(dominanta[j].liczba==tab[i])
            {
                wstawic = false;
                break;
            }
        }
        if(wstawic)
        {
            dominanta[licznik].liczba = tab[i];
            dominanta[licznik].wystapienia = wystapienia;
            licznik++;
        }
        wystapienia = 0;
    }
    return licznik;
}

int max(pizza dominanta[], int rozmiar)
{
    int max = dominanta[0].wystapienia;
    for(int i = 0; i < rozmiar; i++)
    {
        if(max < dominanta[i].wystapienia)
            max = dominanta[i].wystapienia;
    }
    return max;
}
