// Jędrzej Stasik lista 1

#include <iostream>
#include <cmath>

using namespace std;


int main()
{
    float A, B, C;
    cout << "Podaj parametry A, B, C: ";
    cin >> A >> B >> C;

    float p = (-B)/(2.00*A);
    float delta = pow(B, 2) - (4.00 * A * C);
    float q = (-delta) / (4.00*A);

    cout << "wierzcholek paraboli (" << p << "; " << q << ")" << endl;

    if(delta < 0)
    {
        cout << "nie ma pierwiastkow" << endl;
    }
    else if(delta == 0)
    {
        float x0 = p;
        cout << "x0: " << x0 << endl;
    }
    else
    {
        float x1 = ((-B) - sqrt(delta))/2.00*A;
        float x2 = ((-B) + sqrt(delta))/2.00*A;

        cout << "x1: " << x1 << endl;
        cout << "x2: " << x2 << endl;
    }

    return 0;
}


