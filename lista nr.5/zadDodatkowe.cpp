#include <iostream>
#include <cmath>
#define dl 100

using namespace std;

void wprowadzLiczby(char * oblicz, int * liczby,  int ileLiczb);
void wprowadzZnaki(char * oblicz, char * znaki);
int obliczTo(int * liczby, char * znaki);
void przesunZnaki(char * znaki, int n);
int przesunLiczby(int * liczby, int n, int rozm);

int main()
{
    int i, znaki = 0, ileLiczb = 0, poz = 0;
    int * liczby;
    char * Znaki;
    char * tekst = new char[dl];
    char * oblicz;

    cout << "\nKALKUTRON\n";
    cout << ": ";
    cin.getline(tekst, dl);

    for(i = 0; tekst[i]; i++);
    oblicz = new char[i];

    for(i = 0; tekst[i]; i++)
    {
        if(tekst[i]==42 || tekst[i]==43 || tekst[i]==45 || tekst[i]==47 || (tekst[i]>47 && tekst[i]<58))
        {
            oblicz[poz] = tekst[i];
            poz++;
        }
    }
    delete [] tekst;

    for(i = 0; oblicz[i]; i++)
    {
        if(oblicz[i]==42) znaki++;
        if(oblicz[i]==43) znaki++;
        if(oblicz[i]==45) znaki++;
        if(oblicz[i]==47) znaki++;
    }
    
    ileLiczb = znaki + 1;
    Znaki = new char[znaki];
    liczby = new int[ileLiczb];

    wprowadzLiczby(oblicz, liczby, ileLiczb);
    wprowadzZnaki(oblicz, Znaki);
    ileLiczb = obliczTo(liczby, Znaki);

    cout << "=" << liczby[ileLiczb-1] << endl;


    delete [] oblicz;

    /*for(int i = 0; i < znaki; i++)
    {
        cout << Znaki[i] << endl;
    }*/
    //cout << "wprowadzono " << (int)oblicz[0] << " liczby\n";


    delete [] liczby;
    delete [] Znaki;

    return 0;
}

void wprowadzLiczby(char * oblicz, int * liczby,  int ileLiczb)
{
    int poz = 0, pozPierwszejCyfry = 0, pozZnaku = 0, potega;

    for(int i = 0; i < ileLiczb; i++) 
        liczby[i] = 0;

    for(int i = 0; ; i++)
    {
        if((oblicz[i]==42) || (oblicz[i]==43) || (oblicz[i]==45) || (oblicz[i]==47) || oblicz[i]=='\0')
        {
            pozZnaku = i;
            for(int k = pozPierwszejCyfry; k < pozZnaku; k++)
            {
                potega = (pozZnaku-1) - k;
                liczby[poz] += pow(10, potega) * (((int)oblicz[k])-48);
            }
            poz++;
            pozPierwszejCyfry = pozZnaku+1;
        }
        if(oblicz[i]=='\0')
            break;
    }
}

void wprowadzZnaki(char * oblicz, char * znaki)
{
    int poz = 0;
    for(int i = 0; oblicz[i]; i++)
    {
        if(oblicz[i]==42 || oblicz[i]==43 || oblicz[i]==45 || oblicz[i]==47)
        {
            znaki[poz] = oblicz[i];
            poz++;
        }
    }
}

int obliczTo(int * liczby, char * znaki)
{
    int rozm;
    bool kolejnosc = true;

    for(rozm = 0; znaki[rozm]; rozm++);
    rozm++;
    
    while(kolejnosc)
    {
        kolejnosc = false;
        for(int i = 0; znaki[i]; i++)
        {
            if(znaki[i] == 42)
            {
                liczby[i] *= liczby[i+1];
                rozm = przesunLiczby(liczby, i, rozm);
                przesunZnaki(znaki, i);
                //liczby[i+1] = liczby[i];
                //znaki[i] = '?';
                kolejnosc = true;
                break;
            }
            if(znaki[i] == 47)
            {
                liczby[i+1] = liczby[i] / liczby[i+1];
                liczby[i] = liczby[i+1];
                rozm = przesunLiczby(liczby, i, rozm);
                przesunZnaki(znaki, i);
                //znaki[i] = '?';
                kolejnosc = true;
                break;
            }
            
            // if(znaki[i] == 47)
            // {
            //     liczby[i+1] = liczby[i] / liczby[i+1];
            //     liczby[i] = liczby[i+1];
            //     znaki[i] = '?';
            // }
        }
        
    }
    // cout << "wyszedlem\n";
    
    for(int i = 0; znaki[i]; i++)
    {
        // if(znaki[i] == '?')
        // {
        //     // if(znaki[i+1] == 43)
        //     // {
        //     //     liczby[i+2] += liczby[i];
        //     // }

        //     // if(znaki[i+1] == 45)
        //     // {
        //     //     liczby[i+2] = liczby[i] - liczby[i+2];
        //     // }

        //     if(znaki[i+1] == '\0')
        //     {
        //         liczby[i+1] = liczby[i];
        //     }
        // }

        if(znaki[i] == 43)
        {
            liczby[i+1] += liczby[i];
        }
        if(znaki[i] == 45)
        {
            liczby[i+1] = liczby[i] - liczby[i+1];
        }
    }
    return rozm;
}

void przesunZnaki(char * znaki, int n)
{
    for(int i = n; znaki[i]; i++)
    {
        znaki[i] = znaki[i+1];
    }
    // for(int i = 0; znaki[i]; i++)
    //     cout << znaki[i] << endl;
}

int przesunLiczby(int * liczby, int n, int rozm)
{
    // for(int i = 0; i < rozm; i++)
    // {
    //     cout << "i= " << i << " wartosc= " << liczby[i] << endl;
    // }

    for(int i=n+1; i+1<rozm; i++)
    {
        liczby[i] = liczby[i+1];
    }

    // cout << endl;
    // for(int i = 0; i < rozm-1; i++)
    // {
    //     cout << "i= " << i << " wartosc= " << liczby[i] << endl;
    // }
    // cout << endl;

    return rozm-1;
}