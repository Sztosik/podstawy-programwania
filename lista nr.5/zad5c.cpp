#include <iostream>
#define dl 1000
#define P 1

using namespace std;

int main()
{
    int i;
    char napis[dl];
    cout << "Podaj napis: "; cin.getline(napis, dl);

    for(i = 0; napis[i]; i++)
    {   
        if((napis[i]>64 && napis[i]<90) || (napis[i]>96 && napis[i]<122))
            napis[i] = napis[i] + P;

        if(napis[i] == 90) napis[i] = 97;
        if(napis[i] == 122) napis[i] = 65;
    }

    for(i = 0; napis[i]; i++) cout  << napis[i];
    cout << endl;

    return 0;
}