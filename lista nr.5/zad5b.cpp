#include <iostream>
#define dl 1000

using namespace std;

int main()
{
    int cyfra=0, litera=0, znak=0, i;
    char napis[dl];
    cout << "Podaj napis: "; cin.getline(napis, dl);

    for(i = 0; napis[i]; i++)
    {
        if(napis[i]>47 && napis[i]<58)      cyfra++;

        if((napis[i]>64 && napis[i]<91) || 
           (napis[i]>96 && napis[i]<123))   litera++;

        if((napis[i]>31 && napis[i]<48) || 
           (napis[i]>57 && napis[i]<65) || 
           (napis[i]>90 && napis[i]<97)  || 
           (napis[i]>122 && napis[i]<127))  znak++;
    }
    cout << "Cyfry: " << cyfra << endl;
    cout << "litery: " << litera << endl;
    cout << "znaki: " << znak << endl;

    return 0;
}