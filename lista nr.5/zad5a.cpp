#include <iostream>
#define dl 1000

using namespace std;

int main()
{
    bool duzaLitera = false;
    int i;
    char napis[dl];
    cout << "Podaj napis: "; cin.getline(napis, dl);

    for(i = 0; napis[i]; i++)
    {
        if(napis[i]>64 && napis[i]<91)
            duzaLitera = true;
    }
    if(duzaLitera) cout << "w tym napisie jest jakas duza litera\n";
    else           cout << "w tym napisie nie ma duzej litery\n";

    return 0;
}
