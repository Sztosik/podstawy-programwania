#include <iostream>
#define dl 1000

using namespace std;

char zamienNaDuza(char litera);
char zamienNaMala(char litera);

int main()
{
    int i;
    char napis[dl];
    cout << "Podaj napis: "; cin.getline(napis, dl);

    napis[0] = zamienNaDuza(napis[0]);
    for(i = 1; napis[i]; i++)
    {   
        if(napis[i-1] == 32)
        {
            napis[i] = zamienNaDuza(napis[i]);
            continue;
        }

        napis[i] = zamienNaMala(napis[i]);
    }

    for(i = 0; napis[i]; i++) cout << napis[i];
    cout << endl;

    return 0;
}

char zamienNaDuza(char litera)
{
    if(litera > 96 && litera < 123) return litera - 32;
    else return litera;
}

char zamienNaMala(char litera)
{
    if(litera > 64 && litera < 91) return litera + 32;
    else return litera;
}