#include <stdio.h>
#include <cstdlib>

using namespace std;

int main()
{   
    system("clear");

    for(int i = 32; i < 128; i++)
    {
        printf("%d %c %x %o %s", i, i, i, i, "\n");
    }

    return 0;
}