#include <iostream>
#include <iomanip>
#include <cstdlib>

using namespace std;

int main()
{  
    double pi = 3.1415926536;

    system("clear");

    for(int i = 0; i<11; i++)
    {
        cout << fixed << setprecision(i) << pi << endl;

    }
    
    return 0;
}