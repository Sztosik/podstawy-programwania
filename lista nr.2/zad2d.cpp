 
#include <iostream>
#include <cstdlib>
#define roz 20

using namespace std;

float Max(float tab[]);
float Min(float tab[]);

int main()
{
    float tab[roz], suma = 0, srednia = 0, max, min;

    system("clear");

    for(int i = 0; i<roz; i++)
    {
        cout << "podaj liczbe nr. " << i+1 << ": ";
        cin >> tab[i];
        suma += tab[i];
    }
    srednia = suma/roz;

    max = Max(tab);
    min = Min(tab);

    cout << "Suma: " << suma << endl;
    cout << "Srednia: " << srednia << endl;
    cout << "max: " << max << endl;
    cout << "min: " << min << endl;

    return 0;
}

float Max(float tab[])
{
    float max = tab[0];

    for(int i = 0; i<roz; i++)
    {
        if(tab[i] > max)
        {
            max = tab[i];
        }
    }
    return max;
}

float Min(float tab[])
{
    float min = tab[0];

    for(int i = 0; i<roz; i++)
    {
        if(tab[i] < min)
        {
            min = tab[i];
        }
    }
    return min;
}