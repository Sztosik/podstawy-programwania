#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

void wstaw_z_klawiatury(float *tab, int n);
void wstaw_losowe(float *tab, int n);
void drukuj_tablice(float *tab, int n);
void pomnoz_tablice(float *tab1, float *tab2, float *tab3, int n);

int main()
{
    int rozmiar;
    float *A;
    float *B;
    float *C;

    srand(time(NULL));
    system("clear");
    cout << "ile liczb chcesz wprowadzic: "; cin >> rozmiar;

    //tworzę tablice o zadanym rozmiarze
    A = new float[rozmiar];
    B = new float[rozmiar];
    C = new float[rozmiar];

    //uzupelniam tablice
    wstaw_z_klawiatury(A, rozmiar);
    wstaw_losowe(B, rozmiar);
    pomnoz_tablice(C, B, A, rozmiar);

    //drukuje tablice
    cout << "\nTablica A:\n";
    drukuj_tablice(A, rozmiar);
    cout << "\nTablica B:\n";
    drukuj_tablice(B, rozmiar);
    cout << "\nTablica C:\n";
    drukuj_tablice(C, rozmiar);

    //zwalniam pamięć
    delete[] A;
    delete[] B;
    delete[] C;

    cout << "\n\n";
    return 0;
}

void wstaw_z_klawiatury(float *tab, int n)
{
    for(int i = 0; i < n; i++)
    {
        cout << "Podaj " << i+1 << " na " << n << " elementów: "; cin >> tab[i];
    }
}
void wstaw_losowe(float *tab, int n)
{
    for(int i = 0; i < n; i++)
    {
        tab[i] = rand()%101;
    }
}
void drukuj_tablice(float *tab, int n)
{
    for(int i = 0; i < n; i++)
    {
        cout << "tab[" << i << "] = " << tab[i] << endl;
    }
}
void pomnoz_tablice(float *tab1, float *tab2, float *tab3, int n)
{
    for(int i = 0; i < n; i++)
    {
        tab1[i] = tab2[i] * tab3[i];
    }
}