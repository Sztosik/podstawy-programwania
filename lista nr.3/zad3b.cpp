#include <iostream>
#include <cstdlib>
#define rozmiar 10

using namespace std;

int silnia(int n);

int main()
{
    int tab[rozmiar][3];
    int n = 0;

    system("clear");

    for(int i = 0; i < rozmiar; i++)
    {
        n = i + 1;
        tab[i][0] = n;

        //obliczanie silni
        tab[i][1] = silnia(n);

        //wstawianie kolejnych wyrazów ciągu fibonacziego
        if(n == 1 || n == 2)
            tab[i][2] = 1;
        else
            tab[i][2] = tab[i-1][2] + tab[i-2][2];
    }

    // drukowanie tablicy
    
    cout << left << "n ";
    cout << right << "      SIL";
    cout << right << " FIB";
    cout << endl;

    for(int i = 0; i < rozmiar; i++)
    {
        cout.width(2);
        cout << left << tab[i][0] << " ";
        cout.width(8);
        cout << right << tab[i][1] << " ";
        cout.width(3);
        cout << right << tab[i][2] << " ";
        cout << endl;
    }

    return 0;
}

int silnia(int n)
{
    int silnia = 1;
    for(int i = 1; i <= n; i++)
    {
        silnia *= i;
    }
    return silnia;
}