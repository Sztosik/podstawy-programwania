#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

void wstaw_losowe(double **tab, int w, int k);
void wstaw_z_klawiatury(double **tab, int w, int k);
void drukuj_tab(double**tab, int w, int k);
double ** stworz_tablice(int w, int k);
void zwolnij_pamiec(double **tab, int w);
void drukuj_wynik_dodawania(double **A, double **B, double **C, int w, int k);
double** dodaj_macierze(double **A, double **B, int w, int k);
double** odejmij_macierze(double **A, double **B, int w, int k);
double** pomnoz_macierze(double **A, double **B, int w, int k, int n);
double** transponuj_macierz(double **A, int k, int w);

int main()
{
    srand(time(NULL));
    system("clear");

    double **A;
    double **B;
    int wA, kA, wB, kB, wybor;

    cout << "KALKULATOR MACIERZY\n\n";  
    cout << "Podaj rozmiar macierzy A i B\n";
    cout << "Podaj ilosc wierszy macierzy A: ";     cin >> wA;
    cout << "Podaj ilosc kolumn macierzy A: ";      cin >> kA;
    cout << "Podaj ilosc wierszy macierzy B: ";     cin >> wB;
    cout << "Podaj ilosc kolumn macierzy B: ";      cin >> kB;

    //tworzę dwie dynamiczne tablice dwu wymiarowe
    A = new double*[wA];
    B = new double*[wB];

    for(int i = 0; i < wA; i++)
    {
        A[i] = new double[kA];
    }
    for(int i = 0; i < wB; i++)
    {
        B[i] = new double[kB];
    }

    // wstaw_losowe(A, wA, kA);
    // wstaw_losowe(B, wB, kB);
    cout << "Uzupelnij macierz A:" << endl;
    wstaw_z_klawiatury(A, wA, kA);
    cout << "Uzupelnij macierz B:" << endl;
    wstaw_z_klawiatury(B, wB, kB);


        system("clear");

        cout << "Macierz A:" << endl;
        drukuj_tab(A, wA, kA);
        cout << "\nMacierz B:" << endl;
        drukuj_tab(B, wB, kB);

        cout << "\nWYBIERZ DZIALANIE\n";
        cout << "1. Dodawanie\n";
        cout << "2. Odejmowanie A-B\n";
        cout << "3. Odejmowanie B-A\n";
        cout << "4. Mnozenie\n";
        cout << "5. Transponowanie A\n";
        cout << "6. Transponowanie B\n";
        cin >> wybor;


    switch(wybor)
    {
        case 1:
        {
            system("clear");
            cout << "Wybrano dodawanie\n";
            if(wA == wB && kA == kB)
            {
                double **C = dodaj_macierze(A, B, wA, kA);
                drukuj_wynik_dodawania(A, B, C, wA, kA);
                zwolnij_pamiec(C, wA);
            }
            else cout << "Operacja niedozwolona\n";
            break;
        }
        case 2:
        {
            system("clear");
            cout << "Wybrano odejmowanie A-B\n";
            if(wA == wB && kA == kB)
            {
                double **C = odejmij_macierze(A, B, wA, kA);
                drukuj_wynik_dodawania(A, B, C, wA, kA);
                zwolnij_pamiec(C, wA);
            }
            else cout << "Operacja niedozwolona\n";
            break;
        }
        case 3:
        {
            system("clear");
            cout << "Wybrano odejmowanie B-A\n";
            if(wA == wB && kA == kB)
            {
                double **C = odejmij_macierze(B, A, wA, kA);
                drukuj_wynik_dodawania(A, B, C, wA, kA);
                zwolnij_pamiec(C, wA);
            }
            else cout << "Operacja niedozwolona\n";
            break;
        }
        case 4:
        {
            cout << "Wybrano mnozenie\n";
            if(kA == wB)
            {
                double **C = pomnoz_macierze(A, B, wA, kB, wB);
                cout << "\nMacierz A:" << endl;
                drukuj_tab(A, wA, kA);
                cout << "\nMacierz B:" << endl;
                drukuj_tab(B, wB, kB);
                cout << "\nMacierz wynikowa:" << endl;
                drukuj_tab(C, wA, kB);
                zwolnij_pamiec(C, wA);
            }
            else cout << "Operacja niedozwolona\n";
            break;
        }

        case 5:
        {
            system("clear");
            cout << "Wybrano transponowanie A\n";
            double **C = transponuj_macierz(A, wA, kA);
            cout << "\nMacierz A:" << endl;
            drukuj_tab(A, wA, kA);
            cout << "\nMacierz wynikowa:" << endl;
            drukuj_tab(C, kA, wA);
            zwolnij_pamiec(C, kA);
            break;
        }

        case 6:
        {
            system("clear");
            cout << "Wybrano transponowanie B\n";
            double **C = transponuj_macierz(B, wB, kB);
            cout << "\nMacierz B:" << endl;
            drukuj_tab(B, wB, kB);
            cout << "\nMacierz wynikowa:" << endl;
            drukuj_tab(C, kB, wB);
            zwolnij_pamiec(C, kB);
            break;
        }
        default:
        {
            system("clear");
            cout << "Nieprawidlowy wybor\n";
            break;
        }
    }

    zwolnij_pamiec(A, wA);
    zwolnij_pamiec(B, wB);

    return 0;
}

void wstaw_losowe(double **tab, int w, int k)
{
    for(int i = 0; i < w; i++)
    {
        for(int j = 0; j < k; j++)
        {
            tab[i][j] = rand()%10;
        }
    }
}

void wstaw_z_klawiatury(double **tab, int w, int k)
{
    for(int i = 0; i < w; i++)
    {
        for(int j = 0; j < k; j++)
        {
            cout << "podaj " << i << " wiersz i " << j << " kolumne: ";
            cin >> tab[i][j];
        }
    }
}

void drukuj_tab(double **tab, int w, int k)
{
    for(int i = 0; i < w; i++)
    {
        for(int j = 0; j < k; j++)
        {
            cout.width(4);
            cout << right << tab[i][j] << " ";
        }
        cout << "\n\n";
    }
}

void drukuj_wynik_dodawania(double **A, double **B, double **C, int w, int k)
{
    cout << "\nMacierz A:" << endl;
    drukuj_tab(A, w, k);
    cout << "\nMacierz B:" << endl;
    drukuj_tab(B, w, k);
    cout << "\nMacierz wynikowa:" << endl;
    drukuj_tab(C, w, k);
}

void zwolnij_pamiec(double **tab, int w)
{
    for ( int i = 0; i < w; i++ )
    {
        delete[] tab[ i ];
    }
    delete[] tab;
}

double ** stworz_tablice(int w, int k)
{
    double **C = new double*[w];
    for(int i = 0; i < w; i++)
    {
        C[i] = new double[k];
    }
    return C;
}

double** dodaj_macierze(double **A, double **B, int w, int k)
{
    double **C = stworz_tablice(w, k);
    for(int i = 0; i < w; i++)
    {
        for(int j = 0; j < k; j++)
        {
            C[i][j] = A[i][j] + B[i][j];
        }
    }
    return C;
}

double** odejmij_macierze(double **A, double **B, int w, int k)
{
    double **C = stworz_tablice(w, k);
    for(int i = 0; i < w; i++)
    {
        for(int j = 0; j < k; j++)
        {
            C[i][j] = A[i][j] - B[i][j];
        }
    }
    return C;
}

double** pomnoz_macierze(double **A, double **B, int w, int k, int n)
{
    double **C = stworz_tablice(w, k);
    //wyzeruj tablice
    for(int i = 0; i < w; i++)
    {
        for(int j = 0; j < k; j++)
        {
            C[i][j] = 0;
        }
    }

    for(int i = 0; i < w; i++)
    {
        for(int j = 0; j < k; j++)
        {
            for(int y = 0; y < n; y++)
            {
                C[i][j] = (A[i][y] * B[y][j]) + C[i][j];
            }
        }
    }
    return C;
}
double** transponuj_macierz(double **A, int k, int w)
{
    double **C = stworz_tablice(w, k);

    //zamieniam wiersze z kolumnami
    for(int i = 0; i < w; i++)
    {
        for(int j = 0; j < k; j++)
        {
            C[i][j] = A[j][i];
        }
    }
    return C;
}